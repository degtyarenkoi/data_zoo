from typing import List
from itertools import islice

import pandas as pd
import numpy as np


class BacktestingSlices(object):
    def __init__(self, training_length: int=5, forecasting_length: int=2, skip_length: int=2):
        self.training_length = training_length
        self.forecasting_length = forecasting_length
        self.skip_length = skip_length
        self.full_length = self.training_length + self.forecasting_length

        self.training_end_times = list()
        self.compatible_keys = list()

    def get_df_items_key(self, df):
        """
        Get the key assigned to the sub DataFrame.

        :param df: sub DataFrame
        :return: the key assigned to the sub DataFrame (e.g. 'A' or 'B' or 'C' etc)
        """
        for key in df.keys():
            return key[0]

    def get_compatible_data_frame(self, df_base, df_item):
        """
        Check whether two given DataFrames are compatible or not. By compatible we mean that the common section for both
        has length of training_length + forecasting_length or greater.

        :param df_base: sub DataFrame for comparison
        :param df_item: sub DataFrame for comparison
        :return:  - common section for both DataFrames
                  - list of keys for df_base and df_item
        """
        df_base_key = self.get_df_items_key(df_base)
        df_base = pd.DataFrame(df_base[df_base_key]).reset_index()

        df_item = df_item.reset_index()
        df_item_key = df_item['key'][0]

        df_base_start = df_base['index'][0]
        df_base_end = df_base['index'][len(df_base)-1]
        df_item_start = df_item['index'][0]
        df_item_end = df_item['index'][len(df_item)-1]

        if df_base_start <= df_item_start:
            if df_base_end < df_item_end:
                index = np.where(df_item['index'] == df_base_end)[0][0]
                data = df_item['index'][0:index+1]
            else:
                index = np.where(df_base['index'] == df_base_end)[0][0]
                data = df_item['index'][0:index]
        else:
            if df_base_end < df_item_end:
                index = np.where(df_item['index'] == df_base_end)[0][0]
                data = df_base['index'][0:index+1]
            else:
                index = np.where(df_base['index'] == df_base_end)[0][0]
                data = df_base['index'][0:index]

        if len(data) >= self.full_length:
            return data, [df_base_key, df_item_key]

        return None, None

    def get_training_end_times(self, data_frame, compatible_keys):
        """
        Get and save training end times for two given DataFrames. Also here we store keys of compatible DataFrames and start
        and end dates for both.

        :param data_frame: DataFrame that is given from get_compatible_data_frame method (with common section)
        :param compatible_keys: list of keys for two DataFrames which are compatible
        :return:
        """
        current_index = self.training_length - 1

        while True:
            if current_index + self.forecasting_length in data_frame.index:
                date = str(data_frame[current_index].date())
                self.training_end_times.append(date)

                start_date = str(data_frame[0].date())
                end_date = str(data_frame[len(data_frame) - 1].date())
                start_end_dates = [start_date, end_date]

                self.compatible_keys.append({
                    date: {
                        'keys': compatible_keys,
                        'dates': start_end_dates
                    }
                })

                current_index += self.skip_length
            else:
                break

    def check_compatibility(self, df_dict_items, df_base=None):
        """
        Check the compatibility for each sub DataFrame in the whole DataFrame.

        :param df_dict_items: DataFrame except df_base that we compare for compatibility with df_base itself
        :param df_base: sub DataFrame that we compare with df_dict_items for compatibility
        :return:
        """
        if not isinstance(df_dict_items, dict):
            df_dict_items = dict(df_dict_items)

        if len(df_dict_items) < 1:
            return None

        if df_base is None:
            self.check_compatibility(islice(df_dict_items.items(), 1, None), islice(df_dict_items.items(), 1))
        else:
            df_base = dict(df_base)

            for _, df_item in df_dict_items.items():
                data_frame, compatible_keys = self.get_compatible_data_frame(df_base, df_item)

                if data_frame is not None:
                    self.get_training_end_times(data_frame, compatible_keys)

            self.check_compatibility(islice(df_dict_items.items(), 1, None), islice(df_dict_items.items(), 0, 1))

    def create_training_end_times(self, df: pd.DataFrame) -> List[pd.Timestamp]:
        """
        The given parameters, training_length, forecasting_length, and skip_length and the training end time gives a
        unique and reproducible representation for a backtesting slice.

        :return: A list of possible training end times
        """
        df = df.reset_index()
        df_dict_items = dict(tuple(df.groupby(df.key)))
        self.check_compatibility(df_dict_items)
        self.training_end_times.sort()
        return self.training_end_times

    def get_training_end_time_data(self, training_end_time):
        """
        Get dictionary data that contains training_end_time.

        :param training_end_time: specific training end time that was gathered in get_training_end_times method
        :return: dictionary data that has training_end_time or None if no training_end_time found
        """
        for element in self.compatible_keys:
            if training_end_time in element:
                return element[training_end_time]
        return None

    def materialize_slice(self, df: pd.DataFrame, training_end_time: pd.Timestamp) -> (pd.DataFrame, pd.DataFrame):
        """

        :param df:
        :param training_end_time:
        :return: Returns two DataFrames, train and test. The train DataFrame ends training_end_time and the test DataFrame starts at training_end_time + 1 day
        """
        train_df, test_df = None, None

        df = df.reset_index()
        df_dict_items = dict(tuple(df.groupby(df.key)))

        data = self.get_training_end_time_data(training_end_time)

        if data is not None:
            first_key_df = df_dict_items[data['keys'][0]]
            first_df_start_index = np.where(first_key_df['index'] == data['dates'][0])[0][0]
            first_df_end_index = np.where(first_key_df['index'] == training_end_time)[0][0] + 1

            first_key_train_df = first_key_df[first_df_start_index:first_df_end_index]
            first_key_test_df = first_key_df[first_df_end_index:first_df_end_index + self.forecasting_length]

            second_key_df = df_dict_items[data['keys'][1]]
            second_df_start_index = np.where(second_key_df['index'] == data['dates'][0])[0][0]
            second_df_end_index = np.where(second_key_df['index'] == training_end_time)[0][0] + 1

            second_key_train_df = second_key_df[second_df_start_index:second_df_end_index]
            second_key_test_df = second_key_df[second_df_end_index:second_df_end_index + self.forecasting_length]

            train_df = pd.concat([first_key_train_df, second_key_train_df])
            test_df = pd.concat([first_key_test_df, second_key_test_df])

        return train_df, test_df


if __name__ == '__main__':
    from artificial_datasets import ArtificialDatasets

    dataset = ArtificialDatasets.create_multiple_timeseriesset()
    backtesting_slices = BacktestingSlices(training_length=5, forecasting_length=3, skip_length=1)
    backtesting_slices.create_training_end_times(dataset)

    train_slice_1, test_slice_1 = backtesting_slices.materialize_slice(dataset, backtesting_slices.training_end_times[2])
